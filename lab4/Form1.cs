﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Windows.Forms;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using lab4.dao;
using lab4.domain;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;

namespace lab4
{
    public partial class Form1 : Form
    {
        private ISessionFactory factory;
        //Ссылка на сессию
        private static ISession session;
        //Ссылка на родительскую форму
        private Form1 perent;
        //Метод открытия сессии
        //public static ISession CreateSession()
        //{
        //    var factory = SessionFactory.CreateSessionFactory();
        //    var session = factory.OpenSession();
        //    SessionFactory.BuildSchema(session);
        //    return session;
        //}

        //Метод открытия сессии
        private ISession openSession()
        {
            ISession session = null;
            //Получение ссылки на текущую сборку
            Assembly mappingsAssemly = Assembly.GetExecutingAssembly();
            if (factory == null)
            {
                //Конфигурирование фабрики сессий
                factory = Fluently.Configure()
                .Database(PostgreSQLConfiguration
                .PostgreSQL82.ConnectionString(c => c
                .Host("127.0.0.1")
                .Port(5432)
                .Database("skules4")
                .Username("postgres")
                .Password("postgres")))
                .Mappings(m => m.FluentMappings
                .AddFromAssembly(mappingsAssemly))
                //.ExposeConfiguration(BuildSchema)
                .BuildSessionFactory();
            }
            //Открытие сессии
            session = factory.OpenSession();
            return session;
        }
        //Метод для автоматического создания таблиц в базе данных
        private static void BuildSchema(Configuration config)
        {
            new SchemaExport(config).Create(false, true);
        }


        private Form3 form3 = null;
        private Form4 form4 = null;

        private Form3 getForm3()
        {
            if (form3 == null)
            {
                form3 = new Form3();
            }
            form3.setSession(session);
            form3.setPerent(this);
            return form3;
        }
        private Form4 getForm4()
        {
            if (form4 == null)
            {
                form4 = new Form4();
            }
            form4.setSession(session);
            form4.setPerent(this);
            return form4;
        }


        public Form1()
        {
            InitializeComponent();
        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            int selectedRow = dataGridView1.SelectedCells[0].RowIndex;
            string key = (string)dataGridView1.Rows[selectedRow].Cells[0].Value;
            getForm4().Visible = true;
            getForm4().setButton1Visible(true);
            getForm4().setButton2Visible(false);
            getForm4().setTextBox1Text("");
            getForm4().setTextBox2Text("");
            getForm4().setTextBox4Text("");
            getForm4().setSex("М");
            getForm4().setKey1(key);
        }

        private void toolStripMenuItem2_Click(object sender, EventArgs e)
        {
            int selectedRow = dataGridView1.SelectedCells[0].RowIndex;
            string key1 = (string)dataGridView1.Rows[selectedRow].Cells[0].Value;
            selectedRow = dataGridView2.SelectedCells[0].RowIndex;
            string key2 = (string)dataGridView2.Rows[selectedRow].Cells[0].Value;
            selectedRow = dataGridView2.SelectedCells[0].RowIndex;
            string key3 = (string)dataGridView2.Rows[selectedRow].Cells[1].Value;
            DAOFactory dao = new NHibernateDAOFactory(session);
            IPatientDAO patientDAO = dao.getPatientDAO();
            Patient patient = patientDAO.getPatientByDoctorFirstNameAndLastName(key1, key2, key3);
            Record record = patient.Record;

            patient.Record.Doctor.RecordList.Remove(record);
       
            //patient.Doctor.PatientList.Remove(patient);
            patientDAO.Delete(patient);
            fillDataGridView2(key1);
        }

        private void toolStripMenuItem3_Click(object sender, EventArgs e)
        {
            int selectedRow = dataGridView1.SelectedCells[0].RowIndex;
            string key1 = (string)dataGridView1.Rows[selectedRow].Cells[0].Value;
            selectedRow = dataGridView2.SelectedCells[0].RowIndex;
            string key2 = (string)dataGridView2.Rows[selectedRow].Cells[0].Value;
            selectedRow = dataGridView2.SelectedCells[0].RowIndex;
            string key3 = (string)dataGridView2.Rows[selectedRow].Cells[1].Value;
            DAOFactory dao = new NHibernateDAOFactory(session);
            IPatientDAO patientDAO = dao.getPatientDAO();
            Patient patient = patientDAO.getPatientByDoctorFirstNameAndLastName(key1,key2,key3);
            getForm4().Visible = true;
            getForm4().setTextBox1Text(patient.FirstName);
            getForm4().setTextBox2Text(patient.LastName);
            getForm4().setTextBox4Text(patient.Year.ToString());
            getForm4().setSex(patient.Sex.ToString());
            getForm4().setKey1(key1);
            getForm4().setKey2(key2);
            getForm4().setKey3(key3);
            getForm4().setButton1Visible(false);
            getForm4().setButton2Visible(true);
        }

        private void добавитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            getForm3().Visible = true;
            getForm3().setTextBox1Text("");
            getForm3().setTextBox2Text("");
            getForm3().setTextBox3Text("");
            getForm3().setButton1Visible(true);
            getForm3().setButton2Visible(false);
        }

        private void удалитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            DAOFactory dao = new NHibernateDAOFactory(session);
            IDoctorDAO doctorDao = dao.getDoctorDAO();
            int selectedRow = dataGridView1.SelectedCells[0].RowIndex;
            string key = (string)dataGridView1.Rows[selectedRow].Cells[0].Value;
            DialogResult dr = MessageBox.Show("Удалить доктора?", "",
            MessageBoxButtons.YesNo);
            if (dr == DialogResult.Yes)
            {
                try
                {
                    MessageBox.Show(key);
                    doctorDao.delDoctorByName(key);
                    fillDataGridView1();
                }
                catch (Exception e1)
                {
                    MessageBox.Show(e1.Message);
                }
            }
        }

        private void заменитьToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int selectedRow = dataGridView1.SelectedCells[0].RowIndex;
            string key = (string)dataGridView1.Rows[selectedRow].Cells[0].Value;
            DAOFactory dao = new NHibernateDAOFactory(session);
            IDoctorDAO doctorDAO = dao.getDoctorDAO();
            Doctor doctor = doctorDAO.getDoctorByName(key);
            getForm3().Visible = true;
            getForm3().setKey(key);
            getForm3().setTextBox1Text(doctor.DoctorName);
            getForm3().setTextBox2Text(doctor.Specialty);
            getForm3().setTextBox3Text(doctor.Year.ToString());
            getForm3().setButton1Visible(false);
            getForm3().setButton2Visible(true);
        }

        private void dataGridView1_CellClick(object sender, DataGridViewCellEventArgs e)
        {
            int selectedRow = dataGridView1.SelectedCells[0].RowIndex;
            string key = (string)dataGridView1.Rows[selectedRow].Cells[0].Value;
            fillDataGridView2(key);
        }

        private void подключитьсяКБДToolStripMenuItem_Click(object sender, EventArgs e)
        {
            //session = CreateSession();

            session = openSession();

            fillDataGridView1();
        }

        private void отключитьсяОтБДToolStripMenuItem_Click(object sender, EventArgs e)
        {
            session.Close();
            dataGridView1.Rows.Clear();
            dataGridView2.Rows.Clear();
        }




        //Метод по заполнению dataGridView1
        public void fillDataGridView1()
        {
            dataGridView1.Rows.Clear();
            //Создание экземпляра фабрики NHibernate
            DAOFactory dao = new NHibernateDAOFactory(session);
            //Получение dao группы
            IDoctorDAO doctorDAO = dao.getDoctorDAO();
            //Получение коллекции групп с базы
            List<Doctor> doctorList = doctorDAO.GetAll();
            //Вывод всех групп в таблицу
            foreach (Doctor d in doctorList)
            {
                DataGridViewRow row = new DataGridViewRow();
                DataGridViewTextBoxCell cell1 = new DataGridViewTextBoxCell();
                DataGridViewTextBoxCell cell2 = new DataGridViewTextBoxCell();
                DataGridViewTextBoxCell cell3 = new DataGridViewTextBoxCell();
                cell1.ValueType = typeof(string);
                cell1.Value = d.DoctorName;
                cell2.ValueType = typeof(string);
                cell2.Value = d.Specialty;
                cell3.ValueType = typeof(string);
                cell3.Value = d.Year.ToString();
                row.Cells.Add(cell1);
                row.Cells.Add(cell2);
                row.Cells.Add(cell3);
                dataGridView1.Rows.Add(row);
            }
        }

        //Вывод всех студентов заданной группы в dataGridView2
        public void fillDataGridView2(string key)
        {
            dataGridView2.Rows.Clear();
            //Создание фабрики NHibernate
            DAOFactory dao = new NHibernateDAOFactory(session);
            //Получение dao группы
            IDoctorDAO doctorDAO = dao.getDoctorDAO();
            //Получение студентов заданной группы
            Doctor doctor = doctorDAO.getDoctorByName(key);
            if(doctor == null) return;

            IList<Record> recordList = doctor.RecordList;
            //MessageBox.Show("xz", "size: " + recordList.Count);

            foreach (Record record in recordList)
            {
                Patient s = record.Patient;
                if(s == null) continue;
                DataGridViewRow row = new DataGridViewRow();
                DataGridViewTextBoxCell cell1 = new DataGridViewTextBoxCell();
                DataGridViewTextBoxCell cell2 = new DataGridViewTextBoxCell();
                DataGridViewTextBoxCell cell3 = new DataGridViewTextBoxCell();
                DataGridViewTextBoxCell cell4 = new DataGridViewTextBoxCell();
                cell1.ValueType = typeof(string);
                cell1.Value = s.FirstName;
                System.Console.WriteLine(s.FirstName + "\n");
                cell2.ValueType = typeof(string);
                cell2.Value = s.LastName;
                cell3.ValueType = typeof(string);
                cell3.Value = s.Year;
                cell4.ValueType = typeof(string);
                cell4.Value = s.Sex;
                row.Cells.Add(cell1);
                row.Cells.Add(cell2);
                row.Cells.Add(cell3);
                row.Cells.Add(cell4);
                dataGridView2.Rows.Add(row);
            }


            //IList<Patient> patientList = doctor.PatientList;
            ////IList<Patient> patientList = doctorDAO.getAllPatientsOfDoctor(key);
            //foreach (Patient s in patientList)
            //{
            //    DataGridViewRow row = new DataGridViewRow();
            //    DataGridViewTextBoxCell cell1 = new DataGridViewTextBoxCell();
            //    DataGridViewTextBoxCell cell2 = new DataGridViewTextBoxCell();
            //    DataGridViewTextBoxCell cell3 = new DataGridViewTextBoxCell();
            //    DataGridViewTextBoxCell cell4 = new DataGridViewTextBoxCell();
            //    cell1.ValueType = typeof(string);
            //    cell1.Value = s.FirstName;
            //    System.Console.WriteLine(s.FirstName + "\n");
            //    cell2.ValueType = typeof(string);
            //    cell2.Value = s.LastName;
            //    cell3.ValueType = typeof(string);
            //    cell3.Value = s.Year;
            //    cell4.ValueType = typeof(string);
            //    cell4.Value = s.Sex;
            //    row.Cells.Add(cell1);
            //    row.Cells.Add(cell2);
            //    row.Cells.Add(cell3);
            //    row.Cells.Add(cell4);
            //    dataGridView2.Rows.Add(row);
            //}
        }
    }
}
