﻿using System;
using System.Windows.Forms;
using lab4.dao;
using lab4.domain;
using NHibernate;

namespace lab4
{
    public partial class Form3 : Form
    {

        private ISession session;
        private Form1 perent;
        private string key;

        public void setPerent(Form1 perent)
        {
            this.perent = perent;
        }
        public void setSession(ISession session)
        {
            this.session = session;
        }
        public void setKey(string key)
        {
            this.key = key;
        }

        public void setTextBox1Text(string text)
        {
            this.textBox1.Text = text;
        }
        public void setTextBox2Text(string text)
        {
            this.textBox2.Text = text;
        }
        public void setTextBox3Text(string text)
        {
            this.textBox3.Text = text;
        }
        public void setButton1Visible(bool visible)
        {
            this.button1.Visible = visible;
        }
        public void setButton2Visible(bool visible)
        {
            this.button2.Visible = visible;
        }


        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DAOFactory dao = new NHibernateDAOFactory(session);
            IDoctorDAO doctorDao = dao.getDoctorDAO();

            
            Doctor doctor = new Doctor();
            doctor.DoctorName = textBox1.Text;
            doctor.Specialty = textBox2.Text;
            doctor.Year = Int32.Parse(textBox3.Text);

            doctorDao.SaveOrUpdate(doctor);
            perent.fillDataGridView1();
            this.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DAOFactory dao = new NHibernateDAOFactory(session);
            IDoctorDAO doctorDao = dao.getDoctorDAO();
            Doctor doctor = doctorDao.getDoctorByName(key);

            doctor.DoctorName = textBox1.Text;
            doctor.Specialty = textBox2.Text;
            doctor.Year = Int32.Parse(textBox3.Text);
            doctorDao.SaveOrUpdate(doctor);
            perent.fillDataGridView1();
            this.Visible = false;
        }
    }
}
