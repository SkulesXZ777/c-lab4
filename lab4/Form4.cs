﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using lab4.dao;
using lab4.domain;
using NHibernate;

namespace lab4
{
    public partial class Form4 : Form
    {
        private ISession session;
        private Form1 perent;
        private string key1;
        private string key2;
        private string key3;

        public void setSession(ISession session)
        {
            this.session = session;
        }
        public void setPerent(Form1 perent)
        {
            this.perent = perent;
        }
        public void setKey1(string key1)
        {
            this.key1 = key1;
        }
        public void setKey2(string key2)
        {
            this.key2 = key2;
        }
        public void setKey3(string key3)
        {
            this.key3 = key3;
        }

        public void setTextBox1Text(string text)
        {
            this.textBox1.Text = text;
        }
        public void setTextBox2Text(string text)
        {
            this.textBox2.Text = text;
        }
        public void setTextBox4Text(string text)
        {
            this.textBox4.Text = text;
        }
        public void setButton1Visible(bool visible)
        {
            this.button1.Visible = visible;
        }
        public void setButton2Visible(bool visible)
        {
            this.button2.Visible = visible;
        }

        public void setSex(string sex)
        {
            if (sex.Equals("М"))
            {
                radioButton1.Checked = true;
                radioButton2.Checked = false;
            }
            if (sex.Equals("Ж"))
            {
                radioButton2.Checked = true;
                radioButton1.Checked = false;
            }
        }

        public Form4()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            DAOFactory dao = new NHibernateDAOFactory(session);
            IDoctorDAO doctorDAO = dao.getDoctorDAO();
            Doctor doctor = doctorDAO.getDoctorByName(key1);

            Patient patient = new Patient();
            patient.FirstName = textBox1.Text;
            patient.LastName = textBox2.Text;
            patient.Sex = radioButton2.Checked ? 'Ж' : 'М';
            patient.Year = Int32.Parse(textBox4.Text);


            Record record = new Record();
            record.Patient = patient;
            record.Doctor = doctor;
            record.Date = DateTime.Today.ToShortDateString();

            patient.Record = record;
            doctor.RecordList.Add(record);

            dao.getRecordDAO().SaveOrUpdate(record);


            //doctor.PatientList.Add(patient);
            //patient.Doctor = doctor;

            doctorDAO.SaveOrUpdate(doctor);
            perent.fillDataGridView2(key1);
            this.Visible = false;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            DAOFactory dao = new NHibernateDAOFactory(session);
            IPatientDAO patientDAO = dao.getPatientDAO();
            Patient patient = patientDAO.getPatientByDoctorFirstNameAndLastName(key1, key2, key3);
            patient.FirstName = textBox1.Text;
            patient.LastName = textBox2.Text;
            patient.Sex = radioButton2.Checked?'Ж':'М';
            patient.Year = Int32.Parse(textBox4.Text);
            patientDAO.SaveOrUpdate(patient);
            this.Visible = false;
            perent.fillDataGridView2(key1);
        }
    }
}
