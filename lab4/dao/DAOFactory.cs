﻿namespace lab4.dao
{
    abstract public class DAOFactory
    {
        public abstract IDoctorDAO getDoctorDAO();
        public abstract IPatientDAO getPatientDAO();
        public abstract IRecordDAO getRecordDAO();
    }
  
}