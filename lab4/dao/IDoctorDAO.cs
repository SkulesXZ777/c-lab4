﻿using lab4.domain;
using System.Collections.Generic;
namespace lab4.dao
{
    public interface IDoctorDAO : IGenericDAO<Doctor>
    {
        Doctor getDoctorByName(string doctorName);
        IList<Patient> getAllPatientsOfDoctor(string doctorName);
        void delDoctorByName(string doctorName);
    }
}