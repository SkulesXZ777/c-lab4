﻿using lab4.domain;
namespace lab4.dao
{
    public interface IPatientDAO : IGenericDAO<Patient>
    {
        Patient getPatientByDoctorFirstNameAndLastName(
        string doctorName, string firstName, string LastName);
    }
}