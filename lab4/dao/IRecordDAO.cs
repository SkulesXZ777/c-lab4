﻿using lab4.domain;
namespace lab4.dao
{
    public interface IRecordDAO : IGenericDAO<Record>
    {
    }
}