﻿using System;
using NHibernate;
namespace lab4.dao
{
    public class NHibernateDAOFactory : DAOFactory
    {
        /** NHibernate sessionFactory */
        protected ISession session = null;
        public NHibernateDAOFactory(ISession session)
        {
            this.session = session;
        }

        public override IDoctorDAO getDoctorDAO()
        {
            return new DoctorDAO(session);
        }

        public override IPatientDAO getPatientDAO()
        {
            return new PatientDAO(session);
        }

        public override IRecordDAO getRecordDAO()
        {
            return new RecordDAO(session);
        }
    }
}