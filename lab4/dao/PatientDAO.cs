﻿using System.Collections.Generic;
using System.Windows.Forms;
using lab4.domain;
using NHibernate;
using NHibernate.Criterion;

namespace lab4.dao
{
    public class PatientDAO : GenericDAO<Patient>, IPatientDAO
    {
        public PatientDAO(ISession session) : base(session) { }

        public Patient getPatientByDoctorFirstNameAndLastName(string doctorName, string firstName, string LastName)
        {
     
            ICriteria criteria = session.CreateCriteria(typeof(Patient))
            .Add(Restrictions.Eq("FirstName",firstName))
            .Add(Restrictions.Eq("LastName",LastName));
            IList<Patient> list = criteria.List<Patient>();

            MessageBox.Show("xz", "xz: " + list.Count);

            foreach (Patient p in list)
            {
                if (p.Record.Doctor.DoctorName.Equals(doctorName))//ADD .Record
                {
                    return p;
                }
            }
            return null;
        }
    }
}