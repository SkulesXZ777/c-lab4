﻿using System.Collections.Generic;
using lab4.domain;
using NHibernate;
using NHibernate.Criterion;

namespace lab4.dao
{
    public class RecordDAO : GenericDAO<Record>, IRecordDAO
    {
        public RecordDAO(ISession session) : base(session) { }
    }
}