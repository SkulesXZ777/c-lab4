﻿using System.Reflection;
using FluentNHibernate.Cfg;
using FluentNHibernate.Cfg.Db;
using NHibernate;
using NHibernate.Cfg;
using NHibernate.Tool.hbm2ddl;

namespace lab4.dao
{
    public class SessionFactory
    {
        public static ISessionFactory CreateSessionFactory()
        {
             return Fluently.Configure()
                    .Database(SQLiteConfiguration.Standard.InMemory().ShowSql())
                    .Mappings(m => m.FluentMappings.AddFromAssembly(Assembly.GetExecutingAssembly()))
                    .ExposeConfiguration((c) => SavedConfig = c)
                    .BuildSessionFactory();
        }

        private static Configuration SavedConfig;

        public static void BuildSchema(ISession session)
        {
            var export = new SchemaExport(SavedConfig);
            export.Execute(true, true, false,session.Connection, null);
        }
    }
}
