﻿using System.Collections.Generic;
namespace lab4.domain
{
    public class Doctor : EntityBase
    {
        //private IList<Patient> patientList = new List<Patient>();
        private IList<Record> recordList = new List<Record>();
        public virtual string DoctorName { get; set; }
        public virtual string Specialty { get; set; }
        public virtual int Year { get; set; }
        //public virtual IList<Patient> PatientList
        //{
        //    get { return patientList; }
        //    set { patientList = value; }
        //}
        public virtual IList<Record> RecordList
        {
            get { return recordList; }
            set { recordList = value; }
        }
    }
}