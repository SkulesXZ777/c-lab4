﻿namespace lab4.domain
{
    //Класс базовой сущности
    public abstract class EntityBase
    {
        public virtual long Id { get; set; }
    }
}