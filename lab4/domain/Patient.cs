﻿namespace lab4.domain
{
    public class Patient : EntityBase
    {
        public virtual string FirstName { get; set; }
        public virtual string LastName { get; set; }
        public virtual char Sex { get; set; }
        public virtual int Year { get; set; }
        //public virtual Doctor Doctor { get; set; }
        public virtual Record Record { get; set; }
    }
}