﻿namespace lab4.domain
{
    public class Record : EntityBase
    {
        public virtual string Date { get; set; }
        public virtual Doctor Doctor { get; set; }
        public virtual Patient Patient { get; set; }
    }
}