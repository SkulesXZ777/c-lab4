﻿using FluentNHibernate.Mapping;
using lab4.domain;
namespace lab4.mapping
{
    public class DoctorMap : ClassMap<Doctor>
    {
        public DoctorMap()
        {
            Table("Doctors");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.DoctorName);
            Map(x => x.Specialty);
            Map(x => x.Year);
            //HasMany(x => x.PatientList)
            //.Inverse()
            //.Cascade.All()
            //.KeyColumn("DoctorId");
            HasMany(x => x.RecordList)
           .Inverse()
           .Cascade.All()
           .KeyColumn("RecordId");
        }
    }
}