﻿using FluentNHibernate.Mapping;
using lab4.domain;
namespace lab4.mapping
{
    public class PatientMap : ClassMap<Patient>
    {
        public PatientMap()
        {
            Table("Patients");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.FirstName);
            Map(x => x.LastName);
            Map(x => x.Sex);
            Map(x => x.Year);
            //References(x => x.Doctor, "DoctorId");
            References(x => x.Record,"RecordId");
        }
    }
}