﻿using FluentNHibernate.Mapping;
using lab4.domain;
namespace lab4.mapping
{
    public class RecordMap : ClassMap<Record>
    {
        public RecordMap()
        {
            Table("Records");
            Id(x => x.Id).GeneratedBy.Native();
            Map(x => x.Date);
           // Map(x => x.Doctor);
          //  Map(x => x.Patient);
             References(x => x.Doctor, "DoctorId").Cascade.All();
             References(x => x.Patient, "PatientId").Cascade.All();
          //  Map(x => x.Patient, "PatientId");
        }
    }
}